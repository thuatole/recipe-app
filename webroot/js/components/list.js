const List = ({ recipes, onStoreRecipes }) => {
  const defaultQuery = {
    search: '',
    page: 1,
    pageSize: 5,
  };
  const inputSearchRef = React.useRef(null);
  const [query, setQuery] = React.useState({ ...defaultQuery });
  const [totalPages, setTotalPages] = React.useState(0);

  React.useEffect(() => {
    const { search, page, pageSize } = query;
    axios.get(`http://localhost:8081/recipes?search=${search}&page=${page}&pageSize=${pageSize}`)
      .then(response => {
        const { recipes, totalPages } = response.data;
        setTotalPages(totalPages || 0);
        onStoreRecipes(recipes || []);
      });
  }, [query]);

  const onDeleteRecipe = recipeId => {
    const confirmDialog = confirm("Are you sure to delete this item ?");
    if (confirmDialog) {
      axios.delete(`http://localhost:8081/recipes/${recipeId}`)
        .then(() => {
          onStoreRecipes(recipes.filter(recipe => recipe.id !== recipeId));
        });
    }
  };

  const onSearch = () => {
    const { value = '' } = inputSearchRef.current;
    setQuery({...query, search: value });
  };

  return (
    <div>
      <div className="search-area">
        <input ref={inputSearchRef} type="text"/>
        <button className="app-button" onClick={onSearch} type="button">Apply</button>
      </div>
      <div className="recipe-list">
        {recipes.map(recipe => {
          return (
            <SingleRecipe onDelete={onDeleteRecipe} key={recipe.id} recipe={recipe} />
          );
        })}
      </div>
      <Pagination onChangePage={page => setQuery({ ...query, page })} currentPage={query.page} totalPages={totalPages} />
    </div>
  );
};

const ConnectedList = window.ReactRedux.connect(mapStateToProps, mapDispatchToProps)(List);
