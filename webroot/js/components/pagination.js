const Pagination = ({ onChangePage, currentPage, totalPages }) => {
  const listPages = [];
  for (let i = 1; i <= totalPages; i++) {
    listPages.push(<div className={`item ${currentPage === i ? 'active' : ''}`} onClick={() => onChangePage(i)} key={i}>{i}</div>)
  }
  return (
    <div className="pagination">{listPages}</div>
  );
};
