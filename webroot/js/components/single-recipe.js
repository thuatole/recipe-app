const SingleRecipe = ({ recipe, onDelete }) => {
  return (
    <div className="recipe">
      <div className="header">
        <div className="title">{recipe.title}</div>
        <div>
          <button className="app-button" onClick={() => onDelete(recipe.id)} type="button">Delete</button>
        </div>
      </div>
      <div className='text-truncated description'>{recipe.description}</div>
    </div>
  );
};

SingleRecipe.displayName = 'SingleRecipe';
