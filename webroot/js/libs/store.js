const appState = (state = { recipes: [] }, action) => {
  switch (action.type) {
    case 'STORE_RECIPES':
      return { ...state, recipes: action.payload };
    default:
      return state;
  }
};

const appStore = window.Redux.createStore(appState);

const mapStateToProps = state => {
  return {
    recipes: state.recipes,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onStoreRecipes: recipes => {
      dispatch({
        type: 'STORE_RECIPES',
        payload: recipes,
      })
    }
  }
};

